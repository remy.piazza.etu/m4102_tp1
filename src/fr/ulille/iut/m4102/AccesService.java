package fr.ulille.iut.m4102;

/**
 * Cette classe se trouvera côté service
 * Elle recevra une requête sous forme de chaîne
 * de caractère conforme à votre protocole.
 * Elle transformera cette requête en une
 * invocation de méthode sur le service puis
 * renverra le résultat sous forme de chaîne
 * de caractères.
 */
public class AccesService {
    private AlaChaine alc;
    
    public AccesService() {
	alc = new AlaChaine();
    }

    public String traiteInvocation(String invocation) {
	// ici, il faudra décomposer la chaîne pour retrouver la méthode appelée
	// et les paramètres, réaliser l'invocation sur la classe AlaChaine puis
	// renvoyer le résultat sous forme de chaîne.
	
	return null;
    }
}
